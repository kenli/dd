angular.module('app.controllers', [])

.controller('mainCtrl', function($scope,$location,shareResults,$http,$ionicActionSheet,$cordovaCamera,newsService) {
       $scope.results ={};
        /**
         * 将input转换为decimal
         * @param input 输入值
         * @param decLength 小数部分的最大长度
         * @returns {*}
         */
         function input2Decimal (input,decLength) {
                var arr = input.split(".");

                // 小数部分
                var decPart = arr[1].substr(0,2);
                return arr[0] + "." + decPart;
        }
       function success(data){//获取内存成功
          // alert('22222222');
           $scope.results.totalSpace = input2Decimal((data.total/1000000).toString(),2)+'MB';//转换成MB
           $scope.results.free =  input2Decimal((data.free/1000000).toString(),2)+'MB';
       }
       function error(){//获取内存失败
           $scope.results.totalSpace = 'null';
           $scope.results.free ='null';
       }
	  
	//获取新闻列表 load页面
	$scope.hasValue = true;
    $scope.pageIndex = 1;	
	
	//页面详情页
	newsService.getData("data/newsContent.txt")
	.success(function(data){
	   $scope.newsContent = data;
     }).error(function(ex){
		alert("newscontent error" + ex);
	 })
	.finally(function () {
		
	});
	
	
	//页面新闻列表加载
	newsService.getData("data/newsload.json")
	.success(function(newItems){
	   $scope.items = newItems;
     }).error(function(ex){
		alert("load error");
	 })
	.finally(function () {
		
	});
	
	 //调用摄像机
    $scope.toChangeAvatar = function () {
	
		var hideSheet = $ionicActionSheet.show({
        buttons: [
            {text: '拍照'},
            {text: '从手机相册选择'}
        ],
        cancelText: '取消',
        cancel: function () {
        },
        buttonClicked: function (index) {
            console.log(index);
            if (index == '0') {
                document.addEventListener("deviceready", function () {
                        //拍照
                        var options = {
                            quality: 50,
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: Camera.PictureSourceType.CAMERA,
                            allowEdit: true,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: 100,
                            targetHeight: 100,
                            popoverOptions: CameraPopoverOptions,
                            saveToPhotoAlbum: true,
                            correctOrientation: true
                        };
                    $cordovaCamera.getPicture(options).then(function (imageData) {
                        $scope.imageSrc = "data:image/jpeg;base64," + imageData;
                    }, function (err) {
                        // error
                    });
                }, false);
            } else if (index == '1') {
                document.addEventListener("deviceready", function () {
                    //从手机相册选择
                    var options = {
                        destinationType: Camera.DestinationType.FILE_URI,
                        sourceType: 2,     //设为0或2，调用的就是系统的图库
                        quality: 50,
                        allowEdit: true,
                        targetWidth: 200,
                        targetHeight: 200
                    };
                    $cordovaCamera.getPicture(options).then(function (imageURI) {
                        $scope.imageSrc = imageURI;
                    }, function (err) {
                        // error
                    });

                    //$cordovaCamera.cleanup().then(); // only for FILE_URI
                }, false);

            }
            return true;
        }
    });
		
    };
	 //下拉刷新
    $scope.comment = function () {
		alert($scope.imageSrc);
    };
	
	

    //下拉刷新
    $scope.doRefresh = function () {
        console.log("ion-refresher");
        newsService.getData("data/newsreload.json")
		.success(function(newItems) {
		   $scope.items = newItems;
	    })
		.error(function(ex){
			alert("下拉刷新error");
		})
		.finally(function () {
			$scope.$broadcast('scroll.refreshComplete');
		});
        
    };

    $scope.hasMore = function () {
        return $scope.hasValue;
    }
    //上拉刷新
    $scope.loadMore = function () {
		
        console.log("ion-infinite-scroll");
		
		newsService.getData("data/newsappend.json")
		.success(function (newItems) {
			var moreData = newItems;
			 $scope.items = $scope.items.concat(moreData);
			if ($scope.pageIndex > 9) {
				$scope.hasValue = false;
			}
			$scope.pageIndex++;
			$scope.$broadcast('scroll.infiniteScrollComplete');
		})
		.error(function(ex)
		{
			alert("上拉刷新error");
		})
		
		.finally(function () {
			$scope.$broadcast('scroll.refreshComplete');
		});
		
    }
	   //Refresh end
	   
	   
       /*
       * 测试手机
       * */
   $scope.testCpu = function (){
	   return;
       $scope.results.model = device.model;//手机型号
       $scope.results.platform = device.platform;//操作系统平台
       $scope.results.uuid = device.uuid;//手机uuid
       $scope.results.version = device.version;//Android版本
       $scope.results.manufacturer = device.manufacturer;//手机制造商
       $scope.results.serial =  device.serial;//手机序列号
		//alert("aaabken");
     //  alert($scope.results);
      //检查存储容量
       var options ={
           location:2//只检测内存，不检测外部SD卡
       };
      DiskSpacePlugin.info(options,success,error);
       shareResults.set($scope.results);//设置共享数据
      //跳转到结果页面
      $location.path('/result');
      }
	
})

.controller('resultCtrl', function($scope,shareResults) {
       $scope.results =shareResults.get();//获取数据
       //显示结果
})

.controller('aboutCtrl', function($scope) {

	})
	
.controller('countCtrl', function($scope) {
        $scope.Resume = '开始记步！';
        $scope.footCount = 0;
        var onShake = function () {
            // Fired when a shake is detected
            $scope.footCount++;
            $scope.$apply();//强制执行脏值检查，更新显示
        };

        var onError = function () {
            // Fired when there is an accelerometer error (optional)
            alert("记步启动失败！");
        };

        $scope.startCount = function () {//开始记步or暂定
            // Start watching for shake gestures and call "onShake"
			// with a shake sensitivity of 40 (optional, default 30)
            if($scope.Resume==='开始记步！'){
                $scope.Resume='暂停记步！';
                shake.startWatch(onShake, 8 /*, onError */);//通过感应震动推测是否在走路，此方法不准确，应该有更复杂的检测逻辑。
            }
           else{
                $scope.Resume='开始记步！';
                shake.stopWatch();
                $scope.$apply();//强制执行脏值检查，更新显示
            }
        }

        $scope.endCount = function () {//停止记步
            $scope.footCount = 0;
		   // Stop watching for shake gestures
            shake.stopWatch();
            $scope.$apply();//强制执行脏值检查，更新显示
        }
    })
