angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
      .state('tabsController.news', {
    url: '/news',
    views: {
      'tabNews': {
        templateUrl: 'templates/news.html',
        controller: 'mainCtrl'
      }
    }
  })
      .state('tabsController.login', {
        url: '/login',
        views: {
          'tabLogin': {
            templateUrl: 'templates/login.html',
            controller: 'mainCtrl'
          }
        }
      })
  .state('tabsController.result', {
    url: '/result',
    views: {
      'tabResult': {
        templateUrl: 'templates/result.html',
        controller: 'resultCtrl'
      }
    }
  })
  
  .state('tabsController.newsDetail', {
    url: '/newsDetail',
    views: {
      'tabNewsDetail': {
        templateUrl: 'templates/newsDetail.html',
        controller: 'mainCtrl'
      }
    }
  })

  .state('tabsController.about', {
    url: '/about',
    views: {
      'tabAbout': {
        templateUrl: 'templates/about.html',
        controller: 'aboutCtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/tabs',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

$urlRouterProvider.otherwise('/tabs/news')

  

});