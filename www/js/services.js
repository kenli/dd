angular.module('app.services', [])

.factory('shareResults', [function(){//测试页面和结果页面通过service传递数据
        var savedData = {}
        function set(data) {
            savedData = data;
        }
        function get() {
            return savedData;
        }

        return {
            set: set,
            get: get
        }

}])
/*
.service('BlankService', [function(){

}]);
*/
.service('newsService',['$http',function($http){
return {
    getData:function(url){
        return $http.get(url);
    }
}
}]);






